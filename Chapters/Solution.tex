\section{Solution}
\subsection{Definitions}
\begin{itemize}
\item \textbf{Seigniorage Share System} Structure where a stable value
  coin is used in symbiosis with a speculative value coin to split
  transactional value from investment value.
\item \textbf{Rosetta Share} Rosetta's implementation of a speculative
  seigniorage share.
\item \textbf{Rosetta Standard Token} Special token, which will always
  represent a value of an underlying pinned asset, valued in Rosetta
  Shares.
\item \textbf{Smart Bank} A smart contract that sets the exchange rate
  of Rosetta Shares to each Rosetta Standard Token.
\end{itemize}

\subsection{Commodity Based Tokens}
For each asset that is traded on exchanges and has known market prices,
a Rosetta Standard Token can be created. Let's introduce one here, for
the purpose of demonstration, it will be used in the rest of the white
paper: The Rosetta Gold Standard. Its aiming price will be the price
of one ounce of gold. The price will be discovered using ChainLink,
and will be fetched from the largest global exchanges continuously.
After which the weighted mean price will be taken as the current
aiming price.

\paragraph{}
Seigniorage Shares will be used to adjust the value of the Rosetta Gold
Standard to the Gold Standard. This is a structure outlined by Robert
Sams\footnote{\url{https://bravenewcoin.com/assets/Whitepapers/A-Note-on-Cryptocurrency-Stabilisation-Seigniorage-Shares.pdf}},
and improved upon by Vitalik
Buterin\footnote{\url{https://blog.ethereum.org/2014/11/11/search-stable-cryptocurrency/}}
in the search for a stable value token. The seigniorage share is a
shadow currency that is traded on the free market. The value of the
share is not stable, but only used as a reference. Rosetta's
implementation of seigniorage shares are called Rosetta Shares.

\paragraph{}
To issue a Rosetta Gold Standard token, the amount of Rosetta Shares
that is currently worth the same as an ounce of gold have to be bought
on the free market. These Rosetta Shares have to be sent to the smart
contract called the Smart Bank. This smart contract will have a real-
time view of exchange rates, and will issue a single Rosetta Gold
Standard Token. The Rosetta Shares which were used to buy the token
will be burned.

\paragraph{}
To sell a Rosetta Gold Standard Token, the Smart Bank can issue the
same amount of Rosetta Shares as are worth an ounce of gold. These
Rosetta Shares will be newly issued, whereas the Rosetta Gold Standard
token will be burned.

\paragraph{}
The Rosetta Standard Tokens are also freely tradable and do not have to
be issued and burned at the Smart Bank. The Smart Bank will prevent a
Rosetta Standard Token from deviating in price compared to the asset
that it has its value pinned to. The following mechanism makes sure of
that: when the free market prices deviate from the aimed price, traders
will be able to freely use the bank to buy from and sell to at the
aimed price, therefore the free-market price will adjust to the aimed
price.

\subsection{Multi Market Seigniorage Economy}
Both Sams and Buterin argue that a market with a single stable
currency is vulnerable for hyperinflation when interest is greatly
reduced. However, there is no reason to stay with one token only, and
adding more aiming prices will be of a positive influence for the
network. Using more than one index, provides a few improvements over
the single market implementation. These improvements are stated below
and are as follows:

\begin{itemize}
\item Using more indexes improves overall demand for Rosetta Shares,
  because investing in more assets will be possible. If the total
  capital for Rosetta Standard Tokens has to grow, this can only be
  done by issuing them at the Smart Bank in exchange for Rosetta Shares.
\item Using multiple indexes also enables more mutual trading markets
  between Rosetta Standard Tokens, lowering the creation and removal of
  Rosetta Shares, which reduces the volatility of the shares.
\item Using multiple markets reduces the impact on Rosetta Shares by a
  sell-off of a single market of Rosetta Standard Tokens. All Rosetta
  Standard Tokens of that market can be sold at the Smart Bank for
  Rosetta Shares, guaranteeing pay-back through the strength of the
  network, although it would increase the supply of Rosetta Shares. The
  shares will only be printed until the whole market cap of the Rosetta
  Standard Token is sold at the Smart Bank. The sell-off won't influence
  the prices of the other Rosetta Standard Tokens, because their
  exchange rate to Rosetta Shares adjusts automatically when the price
  of the shares drops.
\end{itemize}

Any measurable market can be used to create a Rosetta Standard Token.
Many different tokens can be created, for example: a gold token, a
silver token, an oil token, a rice token, a dollar token, a euro token,
a renminbi token, a ruble token, a won token and a yen token. Many more
tokens can be created to improve the network, but no tokens will be
created that are deemed too risky or short-lived.

\paragraph{Black Swan Event}
When only using a single market Rosetta Standard Token matched to one
Seigniorage share token, the network might collapse when a single
market moves down dramatically. The Rosetta team believes that a
largely diversified set of Rosetta Standard Tokens reduces the risk of
hyperinflation as much as possible, because hyperinflation of the
Rosetta Standard Tokens only happens, when all the Rosetta Standard
Tokens are being exchanged for shares and nobody wants to buy the shares
anymore. With our system, this will not happen when only a single market
collapses. During crashes money moves between markets instead of
disappearing entirely, excluding the losses due to fractional reserve
money systems of course. Therefore a Black Swan Event will only happen
when the complete trust in Rosetta is gone.

\subsection{Rosetta Value Standard}
When a large amount of markets are tokenized, an index of the whole can
be created. This will represent an overview of the world economy, which
allows for the creation of a token, the Rosetta Value Standard, that has
its aiming price based on the index. The aim of the Rosetta Value
Standard is to provide the most stable currency in the world, therefore
the amount of Rosetta Value Standard Tokens will be a good reflection of
the token holders' position within the world economy. The Rosetta team
strongly believes that this is the best way to do so.

Some of the tokens that will be included in the network are displayed in
the diagram below:\\
\includegraphics[scale=0.5]{Chapters/RosettaShares}